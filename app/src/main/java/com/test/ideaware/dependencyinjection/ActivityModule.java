package com.test.ideaware.dependencyinjection;

import com.test.ideaware.view.MainActivity;

import dagger.Module;
import dagger.Provides;

@Module
class ActivityModule {

    @Provides
    MainActivity provideMainActivity(MainActivity activity) {
        return activity;
    }
}
