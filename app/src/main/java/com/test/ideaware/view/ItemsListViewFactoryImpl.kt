package com.test.ideaware.view

import com.test.ideaware.viewmodel.ItemsListViewMvvm
import com.test.ideaware.viewmodel.providers.Injectable

class ItemsListViewFactoryImpl : ItemsListViewFactory {

    override fun getItemsListView(injectables: List<Injectable>): ItemsListViewMvvm.View {
        return ItemsListFragment.newInstance(injectables)
    }
}
