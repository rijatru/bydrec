package com.test.ideaware.viewmodel

interface ListHeaderItemViewMvvm {

    interface ViewModel {

        fun getDate(): String?
    }

    interface View
}
